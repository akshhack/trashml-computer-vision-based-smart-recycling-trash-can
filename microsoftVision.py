import requests as req
import json 

API_END_POINT = "https://southcentralus.api.cognitive.microsoft.com/customvision/v1.1/Prediction/78f39b16-15b5-4fab-9598-c3f2282fa9df/image"
PREDICTION_KEY = "c8defecf7e2647e1866a888b6e578f15"
CONTENT_TYPE = "application/octet-stream"

#TAGS
PREDICTION_TAG = "Predictions"
ITEM_TAG = "Tag"
PROBABILITY_TAG = "Probability"

header = {
            "Content-Type" : CONTENT_TYPE,
            "Prediction-Key" : PREDICTION_KEY
}

def get_image_analysis (image_path):
    '''
    Input: Image path of the image to be analysed.
    Output: Four element array 
            [prediction1Tag, prediction1Probability,
             prediction2Tag, prediction2Probability
            ]
    '''
    with open(image_path, "rb") as f:
        res = req.post(API_END_POINT,
                       headers = header,
                       data = f.read())
        responseString = res.text
        
        jsonObj = json.loads(responseString)
        
        predictionList = jsonObj[PREDICTION_TAG]

        for item in predictionList:
            print("\nTag: " + str(item[ITEM_TAG]))
            print("Probability " + str(item[PROBABILITY_TAG]))
        
        prediction1 = predictionList[0]
        prediction2 = predictionList[1]

        predictions = [
                        prediction1[ITEM_TAG], prediction1[PROBABILITY_TAG],
                        prediction2[ITEM_TAG], prediction2[PROBABILITY_TAG]
                      ]
            
        return predictions
