# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A smart Trash can as our Tartan Hacks project 
* 1.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Create a new branch out of Master.
* Download that branch locally
* Once all commits made and finalized, checkout local master and pull
* Merge master into your local branch and resolve any conflicts
* Make a final push
* Create a pull request of your branch (online) into master

### Google Cloud App Engine ###
Project Id: tartanhacks-194811
Link: https://tartanhacks-194811.appspot.com/trashes

### Contribution guidelines ###

* Please make sure you create a pull request
* Never push on master or make any changes on master

### Who do I talk to? ###

* Akshat Prakash, akshatp@andrew.cmu.edu
# trashml
