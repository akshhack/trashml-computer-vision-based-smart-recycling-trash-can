#!/usr/bin/env python
# Microsoft Azure Computer Vision

import requests
import RPi.GPIO as GPIO
import time
import json
import subprocess

trashList = ["woman", "man", "can", "soda", "paper", "newspaper", "coke", 
             "pepsi", "bottle", "plastic", "coffee", "cup"]

def run():	
	print("Begun init")
	pwm = pi_setup()
	image_counter = 0
	try:
	  while(True):
              #wait_for_event()
              start = time.time()	# time one main cycle
            
              img_path = "/home/pi/Desktop/tartanHacks/smarttrashcangitrepo/images/capture.jpg"

              while take_picture(img_path) == 1:
                  print("Error: Exit status 1. Retrying")

              print("Analysing image")
              image_analysis = cv_request(img_path)
            
              if(is_recyclable(image_analysis)):
              # open_iris and close_iris have waits
                  open_iris(pwm)
                  time.sleep(1)
                  close_iris(pwm)
                  indicate_recyclable(True)
              else:
                  indicate_recyclable(False)

              end = time.time() # time one main cycle
              print("Executed main loop in %s seconds", end - start)
              image_counter = image_counter + 1
        except Exception as e:
              print(e)
        finally:
              clean_up(pwm)

#def wait_for_event():
	# Currently hard-coded to run every 5 seconds
	# time.sleep(5);

def take_picture(img_path):
	cmd = "fswebcam -r '640x480' " + img_path  
	return subprocess.call(cmd, shell = True)

def is_recyclable(jsonData):
	print("Response " + str(jsonData))
	
	keywordList = clean_json_data(jsonData)

	commonality = set(keywordList).intersection(trashList)

	return len(commonality) >= 1

def clean_json_data(jsonData):
	#clean and aggregate json data
	jsonToDict = json.loads(jsonData)
	
	#extract description
	descp = jsonToDict["description"]["captions"][0]["text"]
	descpList = descp.split(" ")
	descpList = [item.lower() for item in descp]

	#extract tags
	tagList = jsonToDict["description"]["tags"]
	tagList = [item.lower() for item in tagList]

	#merge both lists
	finalList = tagList + descpList
	print("\n " + str(finalList) + "\n")
	return finalList

def indicate_recyclable(is_recyclable):
	if (is_recyclable):
		print("is recyclable")
	else:
		print("not recyclable")

def cv_request(img_path):
	# subscription
	subscription_key = "c341ebf912fd42b1bcfdc616b88af5cd"
	assert subscription_key

	# vision analyze
	vision_base_url = "https://eastus.api.cognitive.microsoft.com/vision/v1.0/"

	image_data = open(img_path, "rb").read()

	#put together link
	vision_analyze_url = vision_base_url + "analyze"

	# request template
	headers  = {'Ocp-Apim-Subscription-Key': subscription_key,
				'Content-Type' : 'application/octet-stream'}
	params   = {'visualFeatures': 'Categories,Description,Color,Tags'}
	response = requests.post(vision_analyze_url, headers=headers, params=params, data=image_data)
	response.raise_for_status()
	return response.text

#raspberry pi code
def pi_setup():
   GPIO.setmode(GPIO.BOARD)
   servoPin = 11
   GPIO.setup(servoPin, GPIO.OUT)
   pwm = GPIO.PWM(servoPin, 50)
   print("PWM: " + str(pwm))
   pwm.start(7)
   pwm.ChangeDutyCycle(2)
   return pwm

def setAngle(pwm, angle):
  DC = 1./18. * (angle) + 2
  pwm.ChangeDutyCycle(DC)

def open_iris(pwm):
  setAngle(pwm, 0)
  time.sleep(1.5)

def close_iris(pwm):
  setAngle(pwm, 135)
  time.sleep(1)

def clean_up(pwm):
  pwm.stop()
  GPIO.cleanup()

run()
