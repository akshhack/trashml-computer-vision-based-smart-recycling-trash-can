import requests
import json

API_END_POINT = "https://api.vize.ai/v1/classify/"
API_ACCESS_TOKEN = "Token b47f08f394b6adb1edd1192bc1e4bcd2d6ac0286"
TASK_UUID = "736f17b8-544a-4d8b-ac40-2cf5d0da0b4b"

LABELS_TAG = "labels"
ITEM_TAG = "label_name"
PROBABILITY_TAG = "probability"

headers = {
            "Authorization" : API_ACCESS_TOKEN
}

data = {
        "task" : TASK_UUID        
}

def get_image_analysis (image_path):
    
    with open(image_path, "rb") as f:
        
        files = {
                "image_file" : f
        }
    
        res = requests.post(API_END_POINT,
                            headers = headers,
                            files = files,
                            data = data)
        
        responseString = res.text
        jsonObj = json.loads(responseString)
        
        print("Json object is " + str(jsonObj))

        predictionList = jsonObj[LABELS_TAG]

        prediction1 = predictionList[0]
        prediction2 = predictionList[1]

        predictions = [
                       prediction1[ITEM_TAG], prediction1[PROBABILITY_TAG],
                       prediction2[ITEM_TAG], prediction2[PROBABILITY_TAG]
                      ]

        return predictions
